import 'package:flutter/material.dart';

enum APP_THEME{LIGHT, DARk}
void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatefulWidget {
  const ContactProfilePage({super.key});

  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARk
      ? MyAppTheme.appThemeLight()
      : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget,
        body: buildBodyWidget,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.threesixty),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARk
              ? currentTheme = APP_THEME.LIGHT
              : currentTheme = APP_THEME.DARk;
            });
          },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("pay"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("0914493708"),
    subtitle: Text("Kitty"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTitle() {
  return ListTile(
    leading: Icon(null),
    title: Text("0914493708"),
    subtitle: Text("Mom"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}

Widget mailListTitle() {
  return ListTile(
    leading: Icon(Icons.mail),
    title: Text("6316199@go.buu.ac.th"),
    subtitle: Text("work"),
  );
}

Widget locationListTitle() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("BangKok"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.indigo.shade500,
      onPressed: () {},
    ),
  );
}
Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}

var buildAppBarWidget = AppBar(
  leading: Icon(
    Icons.arrow_back,
  ),
  actions: <Widget>[
    IconButton(
        onPressed: () {
          print("Contact is starred");
        },
        icon: Icon(
          Icons.star_border,
        ))
  ],
);

var buildBodyWidget = ListView(
  children: <Widget>[
    Column(
      children: <Widget>[
        Container(
          width: double.infinity,
          height: 250,
          child: Image.network(
            "https://media.discordapp.net/attachments/971027009549508638/1051167021733265418/79A320DA-5D40-471C-9D87-DE2E2F331028.jpeg?width=573&height=671",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Tanadon Horakul",
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
        Divider(
          color: Colors.black,
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
            ],
          ),
        ),
        
        Divider(color: Colors.grey
        ),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          child: Theme(
            data: ThemeData(
              iconTheme: IconThemeData(
                color: Colors.pink,
              ),
            ),
            child: profileActionItems(),
          ),
        ),
        Divider(color: Colors.grey),
        mobilePhoneListTile(),
        otherPhoneListTitle(),
        mailListTitle(),
        locationListTitle(),
      ],
    )
  ],
);

class MyAppTheme {
  static ThemeData appThemeLight(){
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
            color: Colors.white,
            iconTheme: IconThemeData(
              color: Colors.black
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.indigo.shade500,
        ),
      );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
            color: Colors.black,
            iconTheme: IconThemeData(
              color: Colors.white
            ),
          ),
          iconTheme: IconThemeData(
            color: Colors.pink.shade500,
        ),
      );
  }
}

